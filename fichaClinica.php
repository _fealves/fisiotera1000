<?php
ini_set('display_errors', true); error_reporting(E_ALL);
include_once 'classes/login/conexao.php';
include_once 'classes/db/cadastro_paciente.php';
include_once 'classes/db/ficha_clinica.php';

$busca = new CadastroPaciente;
$dados = $busca->buscaPaciente();

				

if(isset($_POST['ok'])){
	$diagnostico = filter_input(INPUT_POST,"diagnostico",FILTER_SANITIZE_MAGIC_QUOTES);
	$qxprincipal = filter_input(INPUT_POST,"qxprincipal",FILTER_SANITIZE_MAGIC_QUOTES);
	$exfisico = filter_input(INPUT_POST,"exfisico",FILTER_SANITIZE_MAGIC_QUOTES);
	$excomplementares = filter_input(INPUT_POST,"excomplementares",FILTER_SANITIZE_MAGIC_QUOTES);
	$medicamentos = filter_input(INPUT_POST,"medicamentos",FILTER_SANITIZE_MAGIC_QUOTES);
	$id_pacientes = filter_input(INPUT_POST,"pacientes",FILTER_SANITIZE_MAGIC_QUOTES);

	if($_POST['ok']){
		$cadastrar = new FichaClinica;
		$cadastrar->setDiagnostico($diagnostico);
		$cadastrar->setQxPrincipal($qxprincipal);
		$cadastrar->setExFisico($exfisico);
		$cadastrar->setExComplementares($excomplementares);
		$cadastrar->setMedicamentos($medicamentos);
		$cadastrar->setIdPaciente($id_pacientes);

		if($cadastrar->cadastro()){
		echo "Ficha cadastrada com sucesso";
		}
		else{
			echo "Ficha não cadastrada";
		}
	}	
}


?>
<div class="container">
	<div class="form">
		<form method="POST" action="">
			<fieldset>
			<legend>Paciente</legend>
				<select name="pacientes" class="paciente">
					<option value="Paciente" selected>Pacientes</option>
					<?php
						foreach ($dados as $cliente) {
					?>	
					<option value='<?php echo $cliente['id_pacientes']?>'><?php echo $cliente['nome']?></option>	
					<?php	
						}
					?>
					
				</select>
			</fieldset>
			<fieldset>
			<legend>Diagnostico</legend>
				<p><textarea name="diagnostico"></textarea></p>
			</fieldset>
			<fieldset>
			<legend>Queixa Principal</legend>	
				<p><textarea name="qxprincipal"></textarea></p>
			</fieldset>
			<fieldset>
			<legend>Exame Fisico</legend>
				<p><textarea name="exfisico"></textarea></p>
			</fieldset>
			<fieldset>
			<legend>Exames Complementares</legend>	
				<p><textarea name="excomplementares"></textarea></p>
			</fieldset>
			<fieldset>
			<legend>Medicamentos</legend>	
				<p><textarea name="medicamentos"></textarea></p>
			</fieldset>
			<input type="submit" value="Cadastrar" name="ok" class="btn">
		</form>
	</div>
</div>