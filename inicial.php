<?php
session_start();

require_once 'classes/login/conexao.php';
require_once 'classes/login/login.php';

if(isset($_GET['logout'])){
	if($_GET['logout'] == 'ok'){
		Login::deslogar();
	}
}
if(isset($_SESSION['logado'])){
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Fisiotera1000</title>
	<link rel="stylesheet" type="text/css" href="css/estilo-inicial.css">
	<meta charset="UTF-8">
</head>
<body>
	<div class="topo">
		<a>FISIOTERA1000</a>
		bem vindo(a) <?php echo $_SESSION['login']; ?>
		<a href="inicial.php?logout=ok">Sair</a>
	</div>
	<div class="esquerda">
		<ul>
			<li><a href="inicial.php?link=1">Cadastro Paciente</a></li>
			<li><a href="inicial.php?link=2">Ficha clinica</a></li>
			<li><a href="inicial.php?link=3">Histórico paciente</a></li>
		</ul>
	</div>
	<div class="direita">
		<?php
			$link = $_GET['link'];

			$pag[1] = 'cd-paciente.php';
			$pag[2] = 'fichaClinica.php';
			$pag[3] = 'historico.php';

			if(!empty($pag[$link])){
				if(file_exists($pag[$link])){
					include $pag[$link];
				}
			}
			
		?>
	</div>	
	<?php	
	}else{
		echo 'VOCE NAO TEM PERMISSAO DE ACESSO';
	}
?>
</body>
</html>