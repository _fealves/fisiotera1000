<?php

include_once 'classes/login/conexao.php';
include_once 'classes/db/cadastro_paciente.php';
include_once 'classes/db/historico_paciente.php';
include_once 'classes/db/ficha_clinica.php';

$busca = new CadastroPaciente;
$dados = $busca->buscaPaciente();
$id = null;
if($_POST['busca']){
	$id_paciente = filter_input(INPUT_POST,"pacientes",FILTER_SANITIZE_MAGIC_QUOTES);
	$historico = new Historico;
	$historico->setIdPaciente($id_paciente);
	$historicoPaciente = $historico->buscar();
}
if($_POST['atualiza']){
	$diagnostico = filter_input(INPUT_POST,"diagnostico",FILTER_SANITIZE_MAGIC_QUOTES);
	$qxprincipal = filter_input(INPUT_POST,"qxprincipal",FILTER_SANITIZE_MAGIC_QUOTES);
	$exfisico = filter_input(INPUT_POST,"exfisico",FILTER_SANITIZE_MAGIC_QUOTES);
	$excomplementares = filter_input(INPUT_POST,"excomplementares",FILTER_SANITIZE_MAGIC_QUOTES);
	$medicamentos = filter_input(INPUT_POST,"medicamentos",FILTER_SANITIZE_MAGIC_QUOTES);
	$id_paciente = filter_input(INPUT_POST,"id_paciente",FILTER_SANITIZE_MAGIC_QUOTES);

	
	$cadastrar = new FichaClinica;
	$cadastrar->setDiagnostico($diagnostico);
	$cadastrar->setQxPrincipal($qxprincipal);
	$cadastrar->setExFisico($exfisico);
	$cadastrar->setExComplementares($excomplementares);
	$cadastrar->setMedicamentos($medicamentos);
	$cadastrar->setIdPaciente($id_paciente);

	$cadastrar->update();
	
}
?>
<div class="container">
	<div class="form">
		<form action="" method="POST">
			<fieldset>
				<legend>Dados Pessoais</legend>
				<select name="pacientes">
					<option value="Paciente" selected>Pacientes</option>
					<?php
						foreach ($dados as $cliente) {
					?>	
					<option value='<?php echo $id = $cliente['id_pacientes']?>'><?php echo $cliente['nome']?></option>	
					<?php	
						}
					?>		
				</select>	
				<input type="submit" name="busca" value="Buscar" class="btn">
			</fieldset>	
		</form>
		<form action="" method="POST">	
			<fieldset>
				<legend>Ficha Clinica</legend>
				<fieldset>
					<legend>Diagnostico</legend>
					<p><textarea name="diagnostico">
						<?php 
							foreach ($historicoPaciente as $paciente){
								echo $paciente['diagnostico'];
						?>
					</textarea></p>
				</fieldset>
				<fieldset>
					<legend>Queixa Principal</legend>	
					<p><textarea name="qxprincipal">
						<?php
							echo $paciente['qxprincipal'];
						?>
					</textarea></p>
				</fieldset>
				<fieldset>
					<legend>Exame Fisico</legend>
					<p><textarea name="exfisico">
						<?php
							echo $paciente['exfisico'];
						?>
					</textarea></p>
				</fieldset>
				<fieldset>
					<legend>Exames Complementares</legend>	
						<p><textarea name="excomplementares">
							<?php
								echo $paciente['excomplementares'];
							?>
						</textarea></p>
				</fieldset>
				<fieldset>
					<legend>Medicamentos</legend>	
						<p><textarea name="medicamentos">
							<?php
								echo $paciente['medicamentos'];
							
							?>
						</textarea></p>
						<input type="hidden" name="id_paciente" value="<?php echo $paciente['id_ficha_clinica']; } ?>">
				</fieldset>
				
				<input type="submit" name="atualiza" value="Atualizar dados" class="btn">	
			</fieldset>
		</form>
	</div>
</div>