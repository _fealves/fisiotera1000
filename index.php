<?php
session_start();
require_once "classes/login/conexao.php";
require_once "classes/login/login.php";

if(isset($_POST['ok'])){
	$login = filter_input(INPUT_POST,"login",FILTER_SANITIZE_MAGIC_QUOTES);
	$senha = filter_input(INPUT_POST,"senha",FILTER_SANITIZE_MAGIC_QUOTES);

	$l = new Login;
	$l->setLogin($login);
	$l->setSenha($senha);
	if($l->logar()){
		header("Location: inicial.php");
	}else{
		$erro = "Acesso negado - Informe usuário e senha correta";
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Fisiotera1000</title>
	<link rel="stylesheet" type="text/css" href="css/estilo-login.css">
</head>
<body>
	<div class="container">
		<div class="login">
			<form action="" method="post">
				<fieldset>
					<legend>Fisiotera1000</legend>
					<div class="esquerda">
						<input type="text" id="login" name="login" placeholder="Login" class="top">
						<input type="text" id="senha" name="senha" placeholder="senha" class="botton">
					</div>
					<div class="direita">	
						<input type="submit" name="ok" value="Logar" class="btn">
					</div>	
				</fieldset>	
			</form>
		</div>		
	</div>
</body>
</html>