<?php
ini_set('display_errors', true); error_reporting(E_ALL);
	include_once 'classes/login/conexao.php';
	include_once 'classes/db/cadastro_paciente.php';

	if(isset($_POST['ok'])){
		$nome = filter_input(INPUT_POST,"nome",FILTER_SANITIZE_MAGIC_QUOTES);
		$sexo = filter_input(INPUT_POST,"sexo",FILTER_SANITIZE_MAGIC_QUOTES);
		$convenio = filter_input(INPUT_POST,"convenio",FILTER_SANITIZE_MAGIC_QUOTES);
		$telResidencial = filter_input(INPUT_POST,"telResidencial",FILTER_SANITIZE_MAGIC_QUOTES);
		$telComercial = filter_input(INPUT_POST,"telComercial",FILTER_SANITIZE_MAGIC_QUOTES);
		$celular = filter_input(INPUT_POST,"celular",FILTER_SANITIZE_MAGIC_QUOTES);
		$cpf = filter_input(INPUT_POST,"cpf",FILTER_SANITIZE_MAGIC_QUOTES);
		$rg = filter_input(INPUT_POST,"rg",FILTER_SANITIZE_MAGIC_QUOTES);
		$cep = filter_input(INPUT_POST,"cep",FILTER_SANITIZE_MAGIC_QUOTES);
		$cidade = filter_input(INPUT_POST,"cidade",FILTER_SANITIZE_MAGIC_QUOTES);
		$bairro = filter_input(INPUT_POST,"bairro",FILTER_SANITIZE_MAGIC_QUOTES);
		$endereco = filter_input(INPUT_POST,"endereco",FILTER_SANITIZE_MAGIC_QUOTES);
		$complemento = filter_input(INPUT_POST,"complemento",FILTER_SANITIZE_MAGIC_QUOTES);
		$profissao = filter_input(INPUT_POST,"profissao",FILTER_SANITIZE_MAGIC_QUOTES);
		$email = filter_input(INPUT_POST,"email",FILTER_SANITIZE_MAGIC_QUOTES);
		$obs = filter_input(INPUT_POST,"obs",FILTER_SANITIZE_MAGIC_QUOTES);
		$dataCadastro = filter_input(INPUT_POST,"datacadastro",FILTER_SANITIZE_MAGIC_QUOTES);

		$cadastrar = new CadastroPaciente;
		
		if($_POST['ok']){
			$cadastrar->setNome($nome);
			$cadastrar->setSexo($sexo);
			$cadastrar->setConvenio($convenio);
			$cadastrar->setTelResidencial($telResidencial);
			$cadastrar->setTelComercial($telComercial);
			$cadastrar->setCelular($celular);
			$cadastrar->setCpf($cpf);
			$cadastrar->setRg($rg);
			$cadastrar->setCep($cep);
			$cadastrar->setCidade($cidade);
			$cadastrar->setBairro($bairro);
			$cadastrar->setEndereco($endereco);
			$cadastrar->setComplemento($complemento);
			$cadastrar->setProfissao($profissao);
			$cadastrar->setEmail($email);
			$cadastrar->setObs($obs);
			$cadastrar->setDataCadastro($dataCadastro);
			
			if($cadastrar->cadastro()){
				echo "Paciente cadastrado com sucesso";
			}
			else{
				echo "Erro ao cadastrar paciente";
			}


		}
	}
?>

	<div class="container">
	    <div class="form">
			<form method="POST" action="">
				<fieldset>
					<legend>Dados Pessoais</legend>
					<p><input type="text" name="nome" placeholder="Nome">
					<input type="text" name="sexo" placeholder="sexo"></p>
					<p><input type="text" name="telResidencial" placeholder="Tel Residencial">
					<input type="text" name="telComercial" placeholder="Tel Comercial">
					<input type="text" name="celular" placeholder="Celular"></p>
					<p><input type="text" name="cpf" placeholder="CPF">
					<input type="text" name="rg" placeholder="RG"></p>
				</fieldset>
				<fieldset>
					<legend>Endereço</legend>
					<input type="text" name="cep" placeholder="Cep">
					<input type="text" name="cidade" placeholder="Cidade">
					<input type="text" name="bairro" placeholder="Bairro">
					<input type="text" name="endereco" placeholder="Endereço">
					<input type="text" name="complemento" placeholder="Complemento">
				</fieldset>
				<fieldset>
					<legend>Dados Complementares</legend>
					<input type="text" name="profissao" placeholder="Profissão">
					<p><input type="text" name="convenio" placeholder="Convenio"></p>
					<input type="text" name="email" placeholder="E-mail">
					<input type="text" name="obs" placeholder="Observações">
					<input type="text" name="datacadastro" class="data" placeholder="Data Cadastro">
				</fieldset>	
				<input type="submit" value="Cadastrar" name="ok" class="btn">
			</form>
		</div>
	</div>	
