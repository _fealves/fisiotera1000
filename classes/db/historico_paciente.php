<?php
	
	class Historico extends Conexao{
		
		
		private $id_pacientes;

		public function setIdPaciente($id_pacientes){
			$this->id_pacientes = $id_pacientes;
		}

		public function getIdPaciente(){
			return $this->id_pacientes;
		}

	 	public function buscar(){
	 		$pdo = parent::getDB();
	 		$busca = $pdo->prepare("SELECT * FROM ficha_clinica WHERE id_pacientes=?;");
	 		$busca->bindValue(1,$this->getIdPaciente());
	 		$busca->execute();
	 		if($busca->rowCount() == 1){
	 			$dados = $busca->fetchAll(PDO::FETCH_ASSOC);
		 		return $dados;
	 		}else{
	 			return false;
	 		}
	 	}	
	}
?>