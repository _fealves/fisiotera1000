<?php

class FichaClinica extends Conexao
{
	private $diagnostico;
	private $qxprincipal;
	private $exfisico;
	private $excomplementares;
	private $medicamentos;
	private $id_pacientes;

	public function setDiagnostico($diagnostico){
		$this->diagnostico = $diagnostico;
	}

	public function getDiagnostico(){
		return $this->diagnostico;
	}

	public function setQxPrincipal($qxprincipal){
		$this->qxprincipal = $qxprincipal;
	}

	public function getQxPrincipal(){
		return $this->qxprincipal;
	}

	public function setExFisico($exfisico){
		$this->exfisico = $exfisico;
	}

	public function getExFisico(){
		return $this->exfisico;
	}

	public function setExComplementares($excomplementares){
		$this->excomplementares = $excomplementares;
	}

	public function getExComplementares(){
		return $this->excomplementares;
	}

	public function setMedicamentos($medicamentos){
		$this->medicamentos = $medicamentos;
	}

	public function getMedicamentos(){
		return $this->medicamentos;
	}

	public function setIdPaciente($id_pacientes){
		$this->id_pacientes = $id_pacientes;
	}

	public function getIdPaciente(){
		return $this->id_pacientes;
	}

	public function cadastro(){
		$pdo = parent::getDB();
		try{
			$insert=$pdo->prepare("INSERT INTO ficha_clinica(id_pacientes,diagnostico,qxprincipal,exfisico,excomplementares,medicamentos)
			VALUES(:id_pacientes,:diagnostico,:qxprincipal,:exfisico,:excomplementares,:medicamentos);");
			$insert->bindValue(":id_pacientes",$this->getIdPaciente());
			$insert->bindValue(":diagnostico",$this->getDiagnostico());
			$insert->bindValue(":qxprincipal",$this->getQxPrincipal());
			$insert->bindValue(":exfisico",$this->getExFisico());
			$insert->bindValue(":excomplementares",$this->getExComplementares());
			$insert->bindValue(":medicamentos",$this->getMedicamentos());
			$executa = $insert->execute();

			if($executa){
				return true;
			}
			else{
				return false;
			}
		}catch(PDOException $e){
			echo "Erro ao cadastrar ". $e->getMessage(); 
		}	
	}

	public function update(){
		$pdo = parent::getDB();
		try{

			$update = $pdo->prepare("UPDATE ficha_clinica SET diagnostico=:diagnostico,qxprincipal=:qxprincipal,exfisico=:exfisico,excomplementares=:excomplementares,medicamentos=:medicamentos
									 WHERE id_ficha_clinica=:id_pacientes;");
			$update->bindValue(":diagnostico",$this->getDiagnostico());
			$update->bindValue(":qxprincipal",$this->getQxPrincipal());
			$update->bindValue(":exfisico",$this->getExFisico());
			$update->bindValue(":excomplementares",$this->getExComplementares());
			$update->bindValue(":medicamentos",$this->getMedicamentos());
			$update->bindValue(":id_pacientes",$this->getIdPaciente());
			$update->execute();
			//echo $update->rowCount();
			

		}catch(PDOException $e){
			echo "Erro ao alterar dados ". $e->getMessage();
		}
	}	
}