<?php

class CadastroPaciente extends Conexao
{
	
	private $nome;
	private $sexo;
	private $convenio;
	private $telResidencial;
	private $telComercial;
	private $celular;
	private $cpf;
	private $rg;
	private $cep;
	private $cidade;
	private $bairro;
	private $endereco;
	private $complemento;
	private $profissao;
	private $email;
	private $obs;
	private $dataCadastro;

	public function setNome($nome){
 		return $this->nome = $nome;
 	}
 	public function getNome(){
 		return $this->nome;
 	}

 	public function setSexo($sexo){
 		return $this->sexo = $sexo;
 	}
 	public function getSexo(){
 		return $this->sexo;
 	}

 	public function setConvenio($convenio){
 		return $this->convenio = $convenio;
 	}
 	public function getConvenio(){
 		return $this->convenio;
 	}

 	public function setTelResidencial($telResidencial){
 		return $this->telResidencial = $telResidencial;
 	}
 	public function getTelResidencial(){
 		return $this->telResidencial;
 	}

 	public function setTelComercial($telComercial){
 		return $this->telComercial = $telComercial;
 	}
 	public function getTelComercial(){
 		return $this->telComercial;
 	}


 	public function setCelular($celular){
 		return $this->celular = $celular;
 	}
 	public function getCelular(){
 		return $this->celular;
 	}

 	public function setCpf($cpf){
 		return $this->cpf = $cpf;
 	}
 	public function getCpf(){
 		return $this->cpf;
 	}

 	public function setRg($rg){
 		return $this->rg = $rg;
 	}
 	public function getRg(){
 		return $this->rg;
 	}

 	public function setCep($cep){
 		return $this->cep = $cep;
 	}
 	public function getCep(){
 		return $this->cep;
 	}

 	public function setCidade($cidade){
 		return $this->cidade = $cidade;
 	}
 	public function getCidade(){
 		return $this->cidade;
 	}

 	public function setBairro($bairro){
 		return $this->bairro = $bairro;
 	}
 	public function getBairro(){
 		return $this->bairro;
 	}

 	public function setEndereco($endereco){
 		return $this->endereco = $endereco;
 	}
 	public function getEndereco(){
 		return $this->endereco;
 	}

 	public function setComplemento($complemento){
 		return $this->complemento = $complemento;
 	}
 	public function getComplemento(){
 		return $this->complemento;
 	}

 	public function setProfissao($profissao){
 		return $this->profissao = $profissao;
 	}
 	public function getProfissao(){
 		return $this->profissao;
 	}

 	public function setEmail($email){
 		return $this->email = $email;
 	}
 	public function getEmail(){
 		return $this->email;
 	}

 	public function setObs($obs){
 		return $this->obs = $obs;
 	}
 	public function getObs(){
 		return $this->obs;
 	}

 	public function setDataCadastro($dataCadastro){
 		return $this->dataCadastro = $dataCadastro;
 	}
 	public function getDataCadastro(){
 		return $this->dataCadastro;
 	}
 	
 	public function cadastro(){
 		$pdo = parent::getDB();
 		try{
	 		$insert=$pdo->prepare("INSERT INTO pacientes(nome,sexo,convenio,telResidencial,telComercial,celular,cpf,rg,cep,cidade,bairro,endereco,complemento,profissao,email,observacao,dataCadastro) 
	 		VALUES(:nome,:sexo,:convenio,:telResidencial,:telComercial,:celular,:cpf,:rg,:cep,:cidade,:bairro,:endereco,:complemento,:profissao,:email,:obs,:dataCadastro);");
	 		$insert->bindValue(":nome",$this->getNome());
	 		$insert->bindValue(":sexo",$this->getSexo());
	 		$insert->bindValue(":convenio",$this->getConvenio());	
	 		$insert->bindValue(":telResidencial",$this->getTelResidencial());
	 		$insert->bindValue(":telComercial",$this->getTelComercial());
	 		$insert->bindValue(":celular",$this->getCelular());
	 		$insert->bindValue(":cpf",$this->getCpf());
	 		$insert->bindValue(":rg",$this->getRg());
	 		$insert->bindValue(":cep",$this->getCep());
	 		$insert->bindValue(":cidade",$this->getCidade());
	 		$insert->bindValue(":bairro",$this->getBairro());
	 		$insert->bindValue(":endereco",$this->getEndereco());
	 		$insert->bindValue(":complemento",$this->getComplemento());
	 		$insert->bindValue(":profissao",$this->getProfissao());
	 		$insert->bindValue(":email",$this->getEmail());
	 		$insert->bindValue(":obs",$this->getObs());
	 		$insert->bindValue(":dataCadastro",$this->getDataCadastro());
	 		$executa=$insert->execute();
	 		if($executa){
	 			return true;
	 		}else{
	 			return false;
	 		}
	 	}catch(PDOException $e){
	 		echo "Erro ao cadastrar ". $e->getMessage();
	 	}
 	}

 	public function buscaPaciente(){

 		$pdo = parent::getDB();
 		try{
	 		$select=$pdo->prepare("SELECT * FROM pacientes");
	 		$select->execute();
	 		if($select->rowCount() > 0){
	 			$dados = $select->fetchAll(PDO::FETCH_ASSOC);
	 			return $dados;
	 		}
	 		else{
	 			return false;
	 		}
	 	}catch(PDOException $e){
	 		echo "Erro ao efetuar a busca ". $e->getMessage();
	 	}	
 		
 	}
}
?>