<?php
 
 class Login extends Conexao{

 	private $login;
 	private $senha;

 	public function setLogin($login){
 		return $this->login = $login;
 	}
 	public function getLogin(){
 		return $this->login;
 	}

 	public function setSenha($senha){
 		return $this->senha = $senha;
 	}
 	public function getSenha(){
 		return $this->senha;
 	}

 	public function logar(){
 		$pdo = parent::getDB(); 
 		$login = $pdo->prepare("SELECT * FROM usuarios WHERE login=? AND senha=?");
 		$login->bindValue(1,$this->getLogin());
 		$login->bindValue(2,$this->getSenha());
 		$login->execute();

 		if($login->rowCount() == 1){
 			$dados = $login->fetch(PDO::FETCH_OBJ);
 			$_SESSION['login'] = $dados->login;
 			$_SESSION['logado'] = true;
 			return true;
 		}
 		else{
 			return false;
 		}
 	}

 	public function deslogar(){
 		if(isset($_SESSION['logado'])){
 			unset($_SESSION['logado']);
 			session_destroy();
 			header("Location: http://localhost/fisiotera1000/");
 		}
 	}
 }
?>